FROM golang:1.10

RUN mkdir -p /go/src/gitlab.com/purio/feedback
WORKDIR /go/src/gitlab.com/purio/feedback
RUN go get github.com/gin-gonic/gin github.com/bluele/slack
COPY recaptcha.go .
COPY feedback.go .

RUN go build -ldflags "-linkmode external -extldflags -static" -a *.go

FROM scratch
COPY --from=0 /go/src/gitlab.com/purio/feedback/feedback /feedback
COPY --from=0 /etc/ssl/certs /etc/ssl/certs
EXPOSE 5000/tcp
VOLUME ["/site"]
CMD ["/feedback"]
