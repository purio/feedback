// example.go
//
// A simple HTTP server which presents a reCaptcha input form and evaulates the result,
// using the github.com/dpapathanasiou/go-recaptcha package.
//
// See the main() function for usage.
package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/bluele/slack"
	"github.com/gin-gonic/gin"
)

func getEnv(key string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	} else {
		return ""
	}
}

func main() {
	var (
		sec_key     = getEnv("CAPTCHA_SECKEY")
		slack_token = getEnv("SLACK_TOKEN")
		channelName = getEnv("SLACK_CHANNEL")
	)
	missing_env := false
	if sec_key == "" {
		missing_env = true
		log.Println("CAPTCHA_SECKEY env variable is missing")
	}
	if slack_token == "" {
		missing_env = true
		log.Println("SLACK_TOKEN env variable is missing")
	}
	if channelName == "" {
		missing_env = true
		log.Println("SLACK_CHANNEL env variable is missing")
	}

	if missing_env {
		os.Exit(1)
	}

	router := gin.Default()
	api := slack.New(slack_token)

	router.GET("/", func(context *gin.Context) {
		context.Redirect(http.StatusMovedPermanently, "/site")
	})

	router.Static("/site", "./site")

	router.GET("/ping", func(context *gin.Context) {
		context.JSON(200, gin.H{"message": "pong"})
	})

	router.POST("/feedback", func(context *gin.Context) {
		var f map[string]string

		// log.Println(reflect.TypeOf(f))

		if err := context.BindJSON(&f); err != nil {
			context.JSON(http.StatusBadRequest, err)
		} else {
			val := f["g-recaptcha-response"]

			// Seckey
			Init(sec_key)
			OK, err := Confirm(context.ClientIP(), val)
			if err != nil {
				log.Printf("Post error: %s\n", err)
				return
			}

			if OK {
				var buffer bytes.Buffer
				for k, v := range f {
					if k != "g-recaptcha-response" {
						buffer.WriteString(fmt.Sprintf("[%s] :%s\n", k, v))
					}
				}

				err := api.ChatPostMessage(channelName, buffer.String(), nil)
				if err != nil {
					panic(err)
				}
			}
			context.JSON(http.StatusOK, f)
		}
	})

	// Listen and serve on 0.0.0.0:8080
	router.Run(":5000")
}
